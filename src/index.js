import Vue from 'vue';
import App from './App.vue';
import store from './store/index';

Vue.filter('capitalise', value => (value ? value.charAt(0).toUpperCase() + value.slice(1) : ''));

// eslint-disable-next-line no-new
new Vue({
  store,
  el: '#app',
  render: h => h(App),
});
