/**
 * Generate a random integer between the specified numbers
 */
function rand(min, max) {
  return min + Math.floor(Math.random() * (max - min + 1));
}

/**
 * Return a random value from the specified array
 */
function arrayRand(array) {
  return array[rand(0, array.length - 1)];
}

module.exports = {
  rand,
  arrayRand,
};
