import { randomNormal } from 'd3-random';
import { climates, factions, planetInclinations as inclinations } from '../data';
import { arrayRand, rand } from '../random';
import generateName from './name';

const techLevelDistribution = randomNormal(3, 2);

export default function generatePlanet() {
  const location = {};
  location.name = generateName('<planet>').name;
  location.climate = arrayRand(climates);
  location.inclination = arrayRand(inclinations);
  location.size = rand(1, 5);

  location.breathable = Math.random() > 0.7;

  location.population = Math.floor(
    Math.random()
    * (location.climate === 'temperate' ? 100 : 10)
    * (location.breathable ? 1000 : 1)
    * (Math.random() > 0.9 ? 1000 : 1)
    * location.size,
  ) * 1000;

  if (Math.random() > 0.3 || location.inclination === 'military') {
    location.allegiance = arrayRand(factions);
  } else {
    location.allegiance = null;
  }

  location.techLevel = Math.round(techLevelDistribution());
  if (location.techLevel < 1) location.techLevel = 1;
  if (location.techLevel > 8) location.techLevel = 8;

  return location;
}
