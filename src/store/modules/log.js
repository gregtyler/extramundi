/* eslint-disable no-param-reassign */
import narratives from '../../narrative';

export default {
  namespaced: true,
  state: {
    plot: {
      to: null,
      departed: null,
      arrives: null,
    },
    history: [],
  },
  getters: {
    current(state) {
      return state.history[state.history.length - 1];
    },
    inTransit(state) {
      return state.plot.to;
    },
  },
  actions: {
    plot({
      commit,
      dispatch,
      getters,
      rootState,
    }, id) {
      // Check we're not already at that location
      if (getters.current && getters.current.id === id) {
        throw new Error('You can\'t plot to your current location');
      }

      // Calculate the distance to travel
      const { locations } = rootState.map;
      const currentLocation = locations.find(l => l.id === getters.current.id);
      const toLocation = locations.find(l => l.id === id);

      // NB: The distance travelled is actually a curve, not a straight line, so
      // this number is too short. It's more noticeable on shorter distances.
      const distance = Math.sqrt(
        ((toLocation.x - currentLocation.x) ** 2) + ((toLocation.y - currentLocation.y) ** 2),
      );

      // Start travelling
      const speed = 1 / 50;
      const travelTime = distance * speed;
      commit('plot', { id, travelTime });

      // When we arrive, trigger arrival steps
      setTimeout(() => dispatch('arrive'), travelTime * 1000);
    },
    arrive({ commit, rootState, state }) {
      const locationId = state.plot.to;
      const location = rootState.map.locations.find(l => l.id === locationId);

      const narrative = narratives.random(location);
      if (narrative) {
        // If a narrative was sent, add it to the planet
        commit('map/setNarrative', { locationId, narrative }, { root: true });
        commit('arrive');
      } else {
        // If no narrative was set, land immediately
        commit('arrive');
        commit('land');
      }
    },
    land({ commit }) {
      commit('land');
    },
    setLocation({ commit }, id) {
      commit('plot', { id, travelTime: 0 });
      commit('arrive');
      commit('land');
    },
  },
  mutations: {
    plot(state, { id, travelTime }) {
      const arrivalTime = new Date();
      arrivalTime.setSeconds(arrivalTime.getSeconds() + travelTime);

      // Plot towards the location
      state.plot.to = id;
      state.plot.departed = new Date();
      state.plot.arrives = arrivalTime;
    },
    arrive(state) {
      // Add log history entry
      state.history.push({
        id: state.plot.to,
        arrived: new Date(),
        landed: null,
      });

      // Reset plot
      state.plot.to = null;
      state.plot.departed = null;
      state.plot.arrives = null;
    },
    land(state) {
      const entry = state.history[state.history.length - 1];
      entry.landed = new Date();
      if (entry.narrative) {
        entry.narrative.isComplete = true;
      }
    },
  },
};
