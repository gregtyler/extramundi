let genID = 0;
const initialState = {
  shape: '0000000000|0000000000|0001110111|1111111111|1111111111',
  rooms: [
    { id: (genID += 1), x: 0, y: 3, code: 'weapons' },
    { id: (genID += 1), x: 0, y: 4, code: 'weapons' },
    { id: (genID += 1), x: 3, y: 2, code: 'weapons' },
    { id: (genID += 1), x: 1, y: 3, w: 2, h: 2, code: 'bridge' },
    { id: (genID += 1), x: 4, y: 2, code: 'shields' },
    { id: (genID += 1), x: 5, y: 2, code: 'shields' },
    { id: (genID += 1), x: 6, y: 4, code: 'bunk' },
    { id: (genID += 1), x: 7, y: 4, code: 'bunk' },
    { id: (genID += 1), x: 4, y: 3, w: 2, code: 'reactor' },
    { id: (genID += 1), x: 7, y: 2, code: 'hold' },
    { id: (genID += 1), x: 8, y: 2, code: 'hold' },
    { id: (genID += 1), x: 9, y: 2, code: 'hold' },
    { id: (genID += 1), x: 7, y: 3, code: 'hold' },
    { id: (genID += 1), x: 8, y: 3, code: 'hold' },
    { id: (genID += 1), x: 9, y: 3, code: 'hold' },
    { id: (genID += 1), x: 8, y: 4, code: 'hold' },
    { id: (genID += 1), x: 9, y: 4, code: 'hold' },
  ],
};

export default {
  namespaced: true,
  state: initialState,
  actions: {
    async moveRoom({ commit, state }, { id, x, y }) {
      const room = state.rooms.find(r => r.id === id);

      // Check every square of the new room footprint
      for (let i = x; i < x + (room.w || 1); i += 1) {
        for (let t = y; t < y + (room.h || 1); t += 1) {
          // Ensure each space is available
          const roomCovering = state.rooms
            .filter(r => r.id !== id)
            .find(r => (
              i >= r.x && i < (r.x + (r.w || 1)) && t >= r.y && t < (r.y + (r.h || 1))
            ));

          if (roomCovering) {
            throw new Error('Can\'t put room here');
          }
        }
      }

      // Perform the mutation
      commit('moveRoom', { id, x, y });
    },
  },
  mutations: {
    moveRoom(state, { id, x, y }) {
      const room = state.rooms.find(r => r.id === id);
      room.x = x;
      room.y = y;
    },
  },
};
