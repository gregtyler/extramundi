/* eslint-disable no-param-reassign */
import crew from './ship/crew';
import cargo from './ship/cargo';
import layout from './ship/layout';

const initialState = {
  name: 'SS Falcon',
  captainName: 'Greg',
  stats: {
    combat: 5,
    defence: 5,
    speed: 5,
  },
  credits: 500,
  fuel: 16,
};

export default {
  namespaced: true,
  state: initialState,
  getters: {
    berths(state) {
      return state.layout.rooms
        .reduce((c, room) => (room.code === 'bunk' ? c + 1 : c), 0);
    },
    capacity(state) {
      return state.layout.rooms
        .reduce((c, room) => (room.code === 'hold' ? c + 2 : c), 0);
    },
    tonnage(state) {
      return state.cargo.reduce((c, { quantity }) => c + quantity, 0);
    },
  },
  actions: {
    buy({ commit, state, getters }, { code, quantity, cost }) {
      // Ensure there's enough money on board
      if (cost * quantity > state.credits) throw new Error('Insufficient funds');

      // Ensure there's enough space on board
      if (getters.tonnage + quantity > getters.capacity) throw new Error('Insufficient space');

      // Commit changes
      commit('spendCredits', Math.ceil(cost * quantity));
      commit('cargo/add', { code, quantity });
    },
    sell({ commit, state }, { code, quantity, value }) {
      // Check stock exists
      const stock = state.cargo.find(item => item.code === code);
      if (!stock || stock.quantity < quantity) throw new Error('Insufficient stock');

      // Commit changes
      commit('cargo/remove', { code, quantity });
      commit('gainCredits', Math.ceil(value * quantity));
    },
    gainCredits({ commit }, amount) {
      commit('gainCredits', amount);
    },
    spendCredits({ commit }, amount) {
      commit('spendCredits', amount);
    },
  },
  mutations: {
    gainCredits(state, amount) {
      // Always round gains down to ensure an integer number
      state.credits += Math.floor(amount);
    },
    spendCredits(state, amount) {
      // Always round losses up to ensure an integer number
      state.credits -= Math.ceil(amount);
    },
  },
  modules: {
    crew,
    cargo,
    layout,
  },
};
