const START = '_start';
const ENTER_STADIUM_PAY = '1';
const ENTER_STADIUM_FREEBIE = '2';
const OFFERED_BET = '10';
const SUPPORT_RED = '11';
const SUPPORT_BLUE = '12';
const GAMEPLAY = '13';
const WIN_BET = '20';
const LEAVE_GAME = '30';
const IGNORE_GAME = '40';

// Reds are underdogs, Blues expected to win

export default {
  suitability({ location }) {
    return location.type === 'planet' && location.population > 1e6 && location.techLevel < 7 && location.techLevel > 1;
  },
  steps: [
    {
      id: START,
      text: ({ location }) => `The people of ${location.name} play a bizarre sport.\nIt's a team game, with six players on each side, and involves ${location.techLevel > 5 ? 'firing energy weapons at a sphere' : 'hitting a sphere with long sticks'} to deflect it into corners of the playing field. Its excitement comes when the players turn their weapons on one another, ${location.techLevel > 5 ? 'projecting opponents and teammates across the stadium' : 'starting all-out fights'}.\nYou arrive before the final game of season, which has drawn massive crowds from across the galaxy.`,
      options: [
        { label: 'Go to the game', to: ({ ship }) => (ship.credits > 100 ? ENTER_STADIUM_PAY : ENTER_STADIUM_FREEBIE) },
        { label: 'Avoid the stadium', to: IGNORE_GAME },
      ],
    },
    {
      id: IGNORE_GAME,
      text: 'Deciding to avoid the crowds, you head back to your ship.\nYou watch the game from the comfort of your bunk, hearing the whoops and cheers ringing across the city outside.',
    },
    {
      id: ENTER_STADIUM_PAY,
      text: ({ crew, location }) => `You ${crew.length ? 'and your crew' : ''} head to the stadium to purchase tickets. With hours before the game, all that's left are seats in the upper tiers, quite far above the action but, the vendor assures you, with an excellent birds-eye view.\nYou fork out the requisite 20¤ and take the ${location.techLevel > 2 ? 'elevator' : 'stairs'} to your seats.\n[danger -20¤]`,
      effect: ({ dispatch }) => dispatch('ship/spendCredits', 20),
      to: OFFERED_BET,
    },
    {
      id: ENTER_STADIUM_FREEBIE,
      text: ({ crew, random }) => `Approaching the stadium ticket office, you see the sign stating the ticket prices.\n"How much!?" ${crew.length ? `${random.arrayRand(crew).name} exclaims` : 'you exclaim, forgetting yourself'}. A nearby captain approaches with a smile.\n"First time?" she asks, "I couldn't help but overhear. I promise it's worth the price though!"\n"Never had the chance before", you explain.\n"Well, tell you what, I'm always telling people to get involved and I guess I should put my money where my mouth is! I've had a good month and I'll buy your tickets."\nTrue to her word, the captain heads to the ticket office and comes back with tickets for ${crew.length ? 'all of your crew' : 'you'}.\n"Enjoy the match!"`,
      to: OFFERED_BET,
    },
    {
      id: OFFERED_BET,
      text: 'In the stands, a merchant comes round offering bets on the match. Plenty of the crowd are getting involved, extoling the virtues of their favourite team.\nThe merchant makes their way over to you.\n"Fancy a flutter?" they ask with a wink. "I\'m offering 2:1 on the Reds and 3:2 on the Blues. You won\'t find better odds anywhere else!"',
      options: [
        { label: 'Bet 10¤ on the Reds', to: SUPPORT_RED },
        { label: 'Bet 10¤ on the Blues', to: SUPPORT_BLUE },
        { label: 'Don\'t place a bet', to: GAMEPLAY },
      ],
    },
    {
      id: SUPPORT_RED,
      text: 'You give the merchant 10¤.\n"On the Reds please!"\n"A brave choice", the merchant says, "but you\'ll get 20¤ if they win".\n[danger -10¤]',
      effect: ({ dispatch, local }) => {
        local.set('team', 'red');
        dispatch('ship/spendCredits', 10);
      },
      to: GAMEPLAY,
    },
    {
      id: SUPPORT_BLUE,
      text: 'You give the merchant 10¤.\n"On the Blues please!"\n"A safe bet", the merchant says, "you\'ll only get 15¤ if they win, but I think you can be pretty confident!".\n[danger -10¤]',
      effect: ({ dispatch, local }) => {
        local.set('team', 'blue');
        dispatch('ship/spendCredits', 10);
      },
      to: GAMEPLAY,
    },
    {
      id: GAMEPLAY,
      text: ({ local, location }) => `The game kicks off to immediate excitement from the crowd. As the teams enter, cheers reign from all sides of the stadium.\nGameplay starts innocently enough, with the red team deftly scoring the first points. The crowd instantly erupts.\nThe blue fight back, deftly navigating the field and using trick plays to catch the reds out.\nPoints go back-and-forth, with both teams clearly inspired by the warmth of the crowd.\n The points are neck-and-neck as the game comes to an end, but chaos erupts as ${location.techLevel > 5 ? `a ${local.get('winner')} player propels their teammate across the field, landing them perfectly to score the final goal` : `a fight breaks out near one corner, and a ${local.get('winner')} player uses the confusion to sweep the sphere into the point-scoring zone`}.\nAfter a thrilling and close game, the ${local.get('winner') === 'red' ? 'Reds' : 'Blues'} come away the victors, with the crowd fully behind them.`,
      effect({ local }) {
        local.set('winner', Math.random() > 2 / 3 ? 'red' : 'blue');
      },
      to: ({ local }) => {
        if (local.get('team')) {
          return (local.get('team') === local.get('winner') ? WIN_BET : LEAVE_GAME);
        }

        return LEAVE_GAME;
      },
    },
    {
      id: WIN_BET,
      text: ({ local }) => `You track down the merchant as the crowds disperse.\n"Congratulations!" they shout, eagerly handing you your winnings. "What a game! And what a result for you too!" They depart with a wink, and head back into the crowd.\n"Congratulations!" you hear again, as they find their next customer.\n[success +${local.get('winner') === 'red' ? 20 : 15}¤]`,
      effect: ({ dispatch, local }) => dispatch('ship/gainCredits', local.get('winner') === 'red' ? 20 : 15),
      to: LEAVE_GAME,
    },
    {
      id: LEAVE_GAME,
      text: ({ location }) => `You leave the stadium in a buoyant mood, knowing you've witnessed some of the best sport in the galaxy. Heading back to your ship, you hear the celebration of the fans long into the night. ${location.name} truly knows how to have a party.`,
    },
  ],
};
