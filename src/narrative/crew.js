// const WALK = 'WALK';
// const FIGHT_CAPTAIN = 'FIGHT_CAPTAIN';
const FIGHT_MATE = 'FIGHT_MATE';

export default {
  steps: [
    {
      id: '_start',
      text: ({ local }) => `Crewmate ${local.get('mate').name} steps forward.\n\n"I'm prepared to face the beast."`,
      effect: ({ local, crew }) => { local.set('mate', crew[0]); },
      options: [
        // { label: 'No-one needs to die', to: WALK },
        // { label: 'I\'ll do it', to: FIGHT_CAPTAIN },
        { label: 'Go on then', to: FIGHT_MATE },
      ],
    },
    {
      id: FIGHT_MATE,
      text: ({ local }) => `${local.get('mate').name} goes to fight the Balrog...`,
    },
  ],
};
