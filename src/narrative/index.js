// Import every narrative
const req = require.context('./', true, /^(.*\.(js$))[^.]*$/im);
const narratives = req.keys()
  .filter(key => !key.endsWith('index.js'))
  .map(key => req(key).default);

function isValid(narrative, location) {
  if (narrative === null) return false;
  if (narrative.isUsed) return false;
  if (typeof narrative.suitability === 'function' && !narrative.suitability({ location })) return false;

  return true;
}

/**
 * Pick a random, suitable narrative for the given location
 */
function random(location) {
  let narrative = null;
  let attempts = 0;

  // Identify what narratives are unused
  const unusedNarratives = narratives.filter(n => !n.isUsed);
  if (!unusedNarratives.length) return null;

  // Try five times to find a suitable narrative
  while (attempts < 5) {
    const randomNarrative = unusedNarratives[Math.floor(Math.random() * unusedNarratives.length)];
    attempts += 1;

    // If the narrative is valid, use it
    if (isValid(randomNarrative, location)) {
      narrative = randomNarrative;
      break;
    }
  }

  // If a narrative was found, return it and mark it as used
  if (narrative) {
    narrative.isUsed = true;
    return narrative;
  }

  // If nothing suitable was found, return null
  return null;
}

export default {
  random,
};
